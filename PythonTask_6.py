n = int(input("Enter an integer: "))
print(10e5)
if n < 1 or n > 10e5:
    print("words should be >1 and <10e5")
    exit()

words = []
count = [1] * n
length_of_words = 0
print("Enter ", n, "words")


for i in range(n):
    word = input()
    length_of_words += len(word)

    #    if length_of_words > 10e6:
    #        break

    if word not in words:
        words.append(word.lower())
    else:
        loc = words.index(word)
        count[loc] += 1

if length_of_words <= 10e6:
    print("Distinct Words: ", len(words))
    print(count[: len(words)])
else:
    print("Length of words exceeded")
    exit()
