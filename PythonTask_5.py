import matplotlib.pyplot as plt
def square(data):
    lst=list()
    for i in range (len(data)):
        lst.append( data[i]**2)
    return lst

def cube(data):
    lst=list()
    for i in range (len(data)):
        lst.append( data[i]**3)
    return lst

data= [1,2,3,4,5,6,7,8,9,10]

plt.plot(data, square(data))
plt.plot(data, cube(data))
plt.legend(["Square","Cube"])
plt.show()