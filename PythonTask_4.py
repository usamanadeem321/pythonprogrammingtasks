import random
import argparse
import json
import os

# If the “-j” flag is passed then your program has to read the value of “iterations” from a JSON file
# placed in the same directory your script is placed. Make sure you use the argparse library for flags
# and JSON format for the JSON file.

c_points = s_points = iter = 0
parser = argparse.ArgumentParser(
    description="This function will calculate approximate value of pi"
)
parser.add_argument(
    "-i", "--iterations", type=int, metavar="", help="No. of random points generated"
)
parser.add_argument(
    "-j", "--jsn", action="store_true", help="Whether to read from json file or not"
)
arguments = parser.parse_args()
iter = arguments.iterations
string_towrite = '{ "iterations":200000 }'

if arguments.jsn:
    print("Reading from JSON file")
    try:
        fhandle = open("jsonfile.json")

    except FileNotFoundError:
        os.system("touch jsonfile.json")
        fhandle = open("jsonfile.json", "w")
        fhandle.write(string_towrite)
        fhandle.close()
        fhandle = open("jsonfile.json")
        data = json.load(fhandle)
        iter = int(data["iterations"])
    else:
        data = json.load(fhandle)
        iter = int(data["iterations"])

for i in range(iter):
    x = random.uniform(-1, 1)
    y = random.uniform(-1, 1)
    s_points += 1
    d = (x * x + y * y) ** 0.5
    if d <= 1:
        c_points += 1

p = 4 * (c_points / s_points)
print("Value of pi: ", p, "at ", iter, "iterations")
# writing pi value in txt file
f = open("pi.txt", "w")
f.write(str(p))
f.close()