import argparse
ud=lr=0

def foo ():
    global ud, lr

    if arguments.up!=None:        
        ud+=arguments.up
    if arguments.down!=None:        
        ud-=arguments.down
    if arguments.left!=None:        
        lr+=arguments.left
    if arguments.right!=None:        
        lr-=arguments.right


parser= argparse.ArgumentParser(description='This function will give  you the distance bw final and starting position')
parser.add_argument('-u', '--up', type= float, metavar='', help= 'Steps in UP direction')
parser.add_argument('-d', '--down', type= float,metavar='', help= 'Steps in DOWN direction')
parser.add_argument('-l', '--left', type= float, metavar='', help= 'Steps in LEFT direction')
parser.add_argument('-r', '--right', type= float, metavar='', help= 'Steps in RIGHT direction')
arguments=parser.parse_args()
foo()

print ("Total Distance here: ", int(((ud*ud) + (lr*lr))**0.5))